package com.example.testgames1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.graphics.Point;

import java.util.ArrayList;
import java.util.Random;

import static java.lang.Double.SIZE;

public class GameEngine extends SurfaceView implements Runnable {

    Point cagePosition;
    Point catPosition;
    Point sparrowPosition;

    private final String TAG = "SPARROW";

    // game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // drawing variables
    private Canvas canvas;
    private Paint paintbrush;
    private SurfaceHolder holder;

    // Screen resolution varaibles
    private int screenWidth;
    private int screenHeight;

    // VISIBLE GAME PLAY AREA
    // These variables are set in the constructor
    int VISIBLE_LEFT;
    int VISIBLE_TOP;
    int VISIBLE_RIGHT;
    int VISIBLE_BOTTOM;
 int score = 0;
 int y;
    // SPRITES
    Square bullet;
    int SQUARE_WIDTH = 100;

    Square enemy;

    Sprite player;
    Sprite sparrow;
    Sprite cat;
    Sprite cage;

    ArrayList<Square> bullets = new ArrayList<Square>();

    // GAME STATS
    int score1 = 0;
    int PLAYER_SPEED = 10;

    public GameEngine(Context context, int screenW, int screenH) {
        super(context);




        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;

        // initalize sprites
        this.bullet = new Square(context, 100, 600, SQUARE_WIDTH);
        this.enemy = new Square(context, 1000, 100, SQUARE_WIDTH);

        catPosition = new Point();
        catPosition.x = 1600;
        catPosition.y = 700;


        // intialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;

        //cat = new Sprite(getContext(),100, 600, SIZE, SIZE);

        // setup visible game play area variables
        this.VISIBLE_LEFT = 20;
        this.VISIBLE_TOP = 10;
        this.VISIBLE_RIGHT = this.screenWidth - 20;
        this.VISIBLE_BOTTOM = (int) (this.screenHeight * 0.8);


        // initalize sprites
        this.player = new Sprite(this.getContext(), 100, 700, R.drawable.player64);
        this.sparrow = new Sprite(this.getContext(), 500, 200, R.drawable.bird64);
        this.cat = new Sprite(this.getContext(), 1600, 700, R.drawable.cat64);
        this.cage = new Sprite(this.getContext(), 1600, 50, R.drawable.cat_cage);
    }

    @Override
    public void run() {
        while (gameIsRunning == true) {
            updateGame();    // updating positions of stuff
            redrawSprites(); // drawing the stuff
            controlFPS();
        }
    }

    boolean catMovingLeft = true;
    boolean cageMovingLeft = true;
    boolean sparrowMoving = true;
    final int CAT_SPEED = 15;
    boolean enemyIsMovingDown = true;


    public void updateGame() {
        if (cageMovingLeft == true) {

            this.cage.setxPosition(this.cage.getxPosition() + 10);
        } else {
            this.cage.setxPosition(this.cage.getxPosition() - 10);
        }

        // update the enemy hitbox
        this.cage.updateHitbox();

        if (this.cage.getxPosition() >= this.screenHeight ) {
            cageMovingLeft = false;
            this.score1 = this.score1 + 1;
        }
        // R2. colliding with top of screen
        if (this.cage.getxPosition() == this.screenHeight/3) {
            cageMovingLeft = true;


        }

        if (catMovingLeft == true) {
            this.cat.setxPosition(this.cat.getxPosition() + 10);
        } else {
            this.cat.setxPosition(this.cat.getxPosition() - 10);
        }

        // update the enemy hitbox
        this.cat.updateHitbox();

        if (this.cat.getxPosition() >= this.screenHeight) {
            catMovingLeft = false;
        }
        // R2. colliding with top of screen
        if (this.cat.getxPosition() < 400) {
            catMovingLeft = true;


        }

        if (sparrowMoving == true) {
            this.sparrow.setxPosition(this.sparrow.getxPosition() + 10);
        } else {
            this.sparrow.setxPosition(this.sparrow.getxPosition() - 10);
        }

        // update the enemy hitbox
        this.sparrow.updateHitbox();

        if (this.sparrow.getxPosition() >= this.screenHeight) {
            sparrowMoving = false;
        }
        // R2. colliding with top of screen
        if (this.sparrow.getxPosition() < 400) {
            sparrowMoving = true;


        }

        if (enemyIsMovingDown == true) {
            this.enemy.setyPosition(this.enemy.getyPosition() + 30);
        }
        else {
            this.enemy.setyPosition(this.enemy.getyPosition() - 30);
        }

        // update the enemy hitbox
        this.enemy.updateHitbox();


        // do collision detection
        // -----------------------
        // R1. colliding with bottom of screen
        if (this.enemy.getyPosition() >= this.screenHeight-400) {
            enemyIsMovingDown = false;
        }
        // R2. colliding with top of screen
        if (this.enemy.getyPosition() < 120 ) {
            enemyIsMovingDown = true;

            if (this.score1 == 5) {
                canvas.drawText("Game Over",700,400,paintbrush);

            }
        }



        // MAKE BULLET MOVE

        // 1. calculate distance between bullet and enemy
        double a = this.enemy.getxPosition() - this.bullet.getxPosition();
        double b = this.enemy.getyPosition() - this.bullet.getyPosition();

        // d = sqrt(a^2 + b^2)

        double d = Math.sqrt((a * a) + (b * b));

        Log.d(TAG, "Distance to enemy: " + d);

        // 2. calculate xn and yn constants
        // (amount of x to move, amount of y to move)
        double xn = (a / d);
        double yn = (b / d);

        // 3. calculate new (x,y) coordinates
        int newX = this.bullet.getxPosition() + (int) (xn * 15);
        int newY = this.bullet.getyPosition() + (int) (yn * 15);
        this.bullet.setxPosition(newX);
        this.bullet.setyPosition(newY);

        // 4. update the bullet hitbox position
        this.bullet.updateHitbox();


        // COLLISION DETECTION FOR BULLET
        // -----------------------------
        // R1: When bullet intersects the enemy, restart bullet position
        if (bullet.getHitbox().intersect(enemy.getHitbox())) {

            // UPDATE THE SCORE
            this.score = this.score + 1;

            // RESTART THE BULLET FROM INITIAL POSITION
            this.bullet.setxPosition(100);
            this.bullet.setyPosition(600);

            // RESTART THE HITBOX
            this.bullet.updateHitbox();
        }

    }

    public void updatePosition() {


       // if (catMovingLeft == true) {
            //catPosition.x = catPosition.x + CAT_SPEED;
       // } else {
           // catPosition.x = catPosition.x - CAT_SPEED;

            //this.cage.setyPosition(this.cage.getyPosition() + CAT_SPEED);
            //this.cat.setyPosition(this.cat.getyPosition() + CAT_SPEED);
            //this.sparrow.setyPosition(this.sparrow.getyPosition() + CAT_SPEED);


        }


        public void outputVisibleArea () {
            Log.d(TAG, "DEBUG: The visible area of the screen is:");
            Log.d(TAG, "DEBUG: Maximum w,h = " + this.screenWidth + "," + this.screenHeight);
            Log.d(TAG, "DEBUG: Visible w,h =" + VISIBLE_RIGHT + "," + VISIBLE_BOTTOM);
            Log.d(TAG, "-------------------------------------");
        }


        public void redrawSprites () {
            if (holder.getSurface().isValid()) {

                // initialize the canvas
                canvas = holder.lockCanvas();
                // --------------------------------

                // set the game's background color
                canvas.drawColor(Color.argb(255, 255, 255, 255));

                // setup stroke style and width
                paintbrush.setStyle(Paint.Style.FILL);
                paintbrush.setStrokeWidth(8);

                // --------------------------------------------------------
                // draw boundaries of the visible space of app
                // --------------------------------------------------------
                paintbrush.setStyle(Paint.Style.STROKE);
                paintbrush.setColor(Color.argb(255, 0, 128, 0));

                canvas.drawRect(VISIBLE_LEFT, VISIBLE_TOP, VISIBLE_RIGHT, VISIBLE_BOTTOM, paintbrush);
                this.outputVisibleArea();

                // --------------------------------------------------------
                // draw player and sparrow
                // --------------------------------------------------------

                // 1. player
                canvas.drawBitmap(this.player.getImage(), this.player.getxPosition(), this.player.getyPosition(), paintbrush);

                // 2. sparrow
                canvas.drawBitmap(this.sparrow.getImage(), this.sparrow.getxPosition(), this.sparrow.getyPosition(), paintbrush);
                canvas.drawBitmap(this.cat.getImage(), this.cat.getxPosition(), this.cat.getyPosition(), paintbrush);
                canvas.drawBitmap(this.cage.getImage(), this.cage.getxPosition(), this.cage.getyPosition(), paintbrush);
                canvas.drawText("Game Over",700,400,paintbrush);
                canvas.drawText("Score:",1500,100,paintbrush);

                // --------------------------------------------------------
                // draw hitbox on player
                // --------------------------------------------------------
                Rect r = player.getHitbox();
                paintbrush.setStyle(Paint.Style.STROKE);
                canvas.drawRect(r, paintbrush);


                // --------------------------------------------------------
                // draw hitbox on player
                // --------------------------------------------------------
                paintbrush.setTextSize(60);
                paintbrush.setStrokeWidth(5);
                String screenInfo = "Screen size: (" + this.screenWidth + "," + this.screenHeight + ")";
                canvas.drawText(screenInfo, 10, 100, paintbrush);

                // --------------------------------
                holder.unlockCanvasAndPost(canvas);
            }

        }

        public void controlFPS () {
            try {
                gameThread.sleep(17);
            } catch (InterruptedException e) {

            }
        }


        // Deal with user input
        @Override
        public boolean onTouchEvent (MotionEvent event){
            int userAction = event.getActionMasked();
            //@TODO: What should happen when person touches the screen?
            if (userAction == MotionEvent.ACTION_DOWN) {
                // user pushed down on screen

                //Log.d(TAG, "The person tapped: (" + event.getX() + "," + event.getY() + ")");


               // this.player.setxPosition(this.player.getxPosition() - 60);
              //  this.cage.setxPosition(this.cage.getxPosition() - 60);


            }

            else if (userAction == MotionEvent.ACTION_UP) {
                // user lifted their finger
                // for pong, you don't need this, so no code is in here
            }
            return true;
        }

        // Game status - pause & resume
        public void pauseGame () {
            gameIsRunning = false;
            try {
                gameThread.join();
            } catch (InterruptedException e) {

            }
        }
        public void resumeGame () {
            gameIsRunning = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

    }

